from django.contrib import admin
from test_api.models import Article, Author, Magazine
# Register your models here.

admin.site.register(Article,)
admin.site.register(Author,)
admin.site.register(Magazine,)
