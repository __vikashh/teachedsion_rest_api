from rest_framework import serializers

from test_api.models import Article, Author, Magazine

class MagazineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Magazine
        fields = '__all__'


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class ArticleSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(many=False,)
    magazine = MagazineSerializer(many=True,)

    class Meta:
        model = Article
        fields = ['author', 'magazine', 'title', 'body', 'id']

    def create(self, validated_data):
        author_data = validated_data.pop('author')
        magazines_data = validated_data.pop('magazine')
        author = Author.objects.create(**author_data)
        article = Article.objects.create(author=author, **validated_data)
        for magazine_data in magazines_data:
            magazine = Magazine.objects.create(**magazine_data)
            article.magazine.add(magazine)
        return article

    def update(self, instance, validated_data):
        magazine_data = validated_data.pop('magazine')
        magazines = (instance.magazine).all()
        magazines = list(magazines)
        instance.title = validated_data.get('title', instance.title)
        instance.body = validated_data.get('body', instance.body)
        instance.save()
        author_data = validated_data.pop('author')
        authors = (instance.author)
        author = authors
        author.first_name = author_data.get('first_name', author.first_name)
        author.last_name = author_data.get('last_name', author.last_name)
        author.experience = author_data.get('experience', author.experience)
        author.save()

        for magazines_data in magazine_data:
            magazine = magazines.pop(0)
            magazine.magazine_name = magazines_data.get('magazine_name',
                                                 magazine.magazine_name)
            magazine.magazine_publisher = magazines_data.get('magazine_publisher',
                               magazine.magazine_publisher)
            magazine.save()
        return instance
