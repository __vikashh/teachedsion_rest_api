from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import status

from test_api.models import Article, Author, Magazine
from test_api.serializers import ArticleSerializer, MagazineSerializer
# Create your views here.

class MagazineList(APIView):
    def get(self, request):
        qs = Magazine.objects.all()
        serializer = MagazineSerializer(qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view()
def author_article(request, id):
    try:
        author = Author.objects.get(id=id)
    except Author.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    qs = Article.objects.filter(author=author)
    serializer = ArticleSerializer(qs, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view()
def magazine_article(request, id):
    try:
        magazine = Magazine.objects.get(id=id)
    except Magazine.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    qs = Article.objects.filter(magazine=magazine)
    serializer = ArticleSerializer(qs, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)

class ArticleViewset(ModelViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
